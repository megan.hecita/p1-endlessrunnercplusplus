// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNER_EndlessRunnerGameMode_generated_h
#error "EndlessRunnerGameMode.generated.h already included, missing '#pragma once' in EndlessRunnerGameMode.h"
#endif
#define ENDLESSRUNNER_EndlessRunnerGameMode_generated_h

#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_SPARSE_DATA
#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_RPC_WRAPPERS
#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEndlessRunnerGameMode(); \
	friend struct Z_Construct_UClass_AEndlessRunnerGameMode_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunnerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner"), ENDLESSRUNNER_API) \
	DECLARE_SERIALIZER(AEndlessRunnerGameMode)


#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAEndlessRunnerGameMode(); \
	friend struct Z_Construct_UClass_AEndlessRunnerGameMode_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunnerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner"), ENDLESSRUNNER_API) \
	DECLARE_SERIALIZER(AEndlessRunnerGameMode)


#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENDLESSRUNNER_API AEndlessRunnerGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlessRunnerGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENDLESSRUNNER_API, AEndlessRunnerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunnerGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENDLESSRUNNER_API AEndlessRunnerGameMode(AEndlessRunnerGameMode&&); \
	ENDLESSRUNNER_API AEndlessRunnerGameMode(const AEndlessRunnerGameMode&); \
public:


#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENDLESSRUNNER_API AEndlessRunnerGameMode(AEndlessRunnerGameMode&&); \
	ENDLESSRUNNER_API AEndlessRunnerGameMode(const AEndlessRunnerGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENDLESSRUNNER_API, AEndlessRunnerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunnerGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEndlessRunnerGameMode)


#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_9_PROLOG
#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_SPARSE_DATA \
	EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_RPC_WRAPPERS \
	EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_INCLASS \
	EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_SPARSE_DATA \
	EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_INCLASS_NO_PURE_DECLS \
	EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNER_API UClass* StaticClass<class AEndlessRunnerGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner_Source_EndlessRunner_EndlessRunnerGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
