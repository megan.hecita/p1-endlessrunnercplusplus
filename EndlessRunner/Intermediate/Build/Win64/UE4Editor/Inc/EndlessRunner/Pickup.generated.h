// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
class ARunCharacter;
#ifdef ENDLESSRUNNER_Pickup_generated_h
#error "Pickup.generated.h already included, missing '#pragma once' in Pickup.h"
#endif
#define ENDLESSRUNNER_Pickup_generated_h

#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_SPARSE_DATA
#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnGetTrigger);


#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnGetTrigger);


#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_EVENT_PARMS \
	struct Pickup_eventOnGet_Parms \
	{ \
		ARunCharacter* player; \
	};


#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_CALLBACK_WRAPPERS
#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPickup(); \
	friend struct Z_Construct_UClass_APickup_Statics; \
public: \
	DECLARE_CLASS(APickup, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(APickup)


#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPickup(); \
	friend struct Z_Construct_UClass_APickup_Statics; \
public: \
	DECLARE_CLASS(APickup, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(APickup)


#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APickup(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APickup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickup(APickup&&); \
	NO_API APickup(const APickup&); \
public:


#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickup(APickup&&); \
	NO_API APickup(const APickup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APickup)


#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StaticMesh() { return STRUCT_OFFSET(APickup, StaticMesh); }


#define EndlessRunner_Source_EndlessRunner_Pickup_h_9_PROLOG \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_EVENT_PARMS


#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_SPARSE_DATA \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_RPC_WRAPPERS \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_CALLBACK_WRAPPERS \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_INCLASS \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner_Source_EndlessRunner_Pickup_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_SPARSE_DATA \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_CALLBACK_WRAPPERS \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_INCLASS_NO_PURE_DECLS \
	EndlessRunner_Source_EndlessRunner_Pickup_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNER_API UClass* StaticClass<class APickup>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner_Source_EndlessRunner_Pickup_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
