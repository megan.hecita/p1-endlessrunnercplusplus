// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATile;
struct FTransform;
class AActor;
class UPrimitiveComponent;
struct FHitResult;
#ifdef ENDLESSRUNNER_Tile_generated_h
#error "Tile.generated.h already included, missing '#pragma once' in Tile.h"
#endif
#define ENDLESSRUNNER_Tile_generated_h

#define EndlessRunner_Source_EndlessRunner_Tile_h_9_DELEGATE \
struct _Script_EndlessRunner_eventExited_Parms \
{ \
	ATile* FloorTile; \
}; \
static inline void FExited_DelegateWrapper(const FMulticastScriptDelegate& Exited, ATile* FloorTile) \
{ \
	_Script_EndlessRunner_eventExited_Parms Parms; \
	Parms.FloorTile=FloorTile; \
	Exited.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define EndlessRunner_Source_EndlessRunner_Tile_h_14_SPARSE_DATA
#define EndlessRunner_Source_EndlessRunner_Tile_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAttachTransform); \
	DECLARE_FUNCTION(execDestroySpawnedObjects); \
	DECLARE_FUNCTION(execSpawnPickup); \
	DECLARE_FUNCTION(execSpawnObstacle); \
	DECLARE_FUNCTION(execExitTrigger);


#define EndlessRunner_Source_EndlessRunner_Tile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAttachTransform); \
	DECLARE_FUNCTION(execDestroySpawnedObjects); \
	DECLARE_FUNCTION(execSpawnPickup); \
	DECLARE_FUNCTION(execSpawnObstacle); \
	DECLARE_FUNCTION(execExitTrigger);


#define EndlessRunner_Source_EndlessRunner_Tile_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define EndlessRunner_Source_EndlessRunner_Tile_h_14_INCLASS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define EndlessRunner_Source_EndlessRunner_Tile_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public:


#define EndlessRunner_Source_EndlessRunner_Tile_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATile)


#define EndlessRunner_Source_EndlessRunner_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Arrow() { return STRUCT_OFFSET(ATile, Arrow); } \
	FORCEINLINE static uint32 __PPO__BoxCollision() { return STRUCT_OFFSET(ATile, BoxCollision); } \
	FORCEINLINE static uint32 __PPO__BoundingBox() { return STRUCT_OFFSET(ATile, BoundingBox); } \
	FORCEINLINE static uint32 __PPO__ObstacleArray() { return STRUCT_OFFSET(ATile, ObstacleArray); } \
	FORCEINLINE static uint32 __PPO__PickupArray() { return STRUCT_OFFSET(ATile, PickupArray); } \
	FORCEINLINE static uint32 __PPO__obstacleSpawn() { return STRUCT_OFFSET(ATile, obstacleSpawn); } \
	FORCEINLINE static uint32 __PPO__pickupSpawn() { return STRUCT_OFFSET(ATile, pickupSpawn); }


#define EndlessRunner_Source_EndlessRunner_Tile_h_11_PROLOG
#define EndlessRunner_Source_EndlessRunner_Tile_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner_Source_EndlessRunner_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner_Source_EndlessRunner_Tile_h_14_SPARSE_DATA \
	EndlessRunner_Source_EndlessRunner_Tile_h_14_RPC_WRAPPERS \
	EndlessRunner_Source_EndlessRunner_Tile_h_14_INCLASS \
	EndlessRunner_Source_EndlessRunner_Tile_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner_Source_EndlessRunner_Tile_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner_Source_EndlessRunner_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner_Source_EndlessRunner_Tile_h_14_SPARSE_DATA \
	EndlessRunner_Source_EndlessRunner_Tile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner_Source_EndlessRunner_Tile_h_14_INCLASS_NO_PURE_DECLS \
	EndlessRunner_Source_EndlessRunner_Tile_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNER_API UClass* StaticClass<class ATile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner_Source_EndlessRunner_Tile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
