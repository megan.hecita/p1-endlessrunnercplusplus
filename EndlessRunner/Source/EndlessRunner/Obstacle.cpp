// Fill out your copyright notice in the Description page of Project Settings.


#include "Obstacle.h"
#include "Engine/StaticMesh.h"
#include "RunCharacter.h"

// Sets default values
AObstacle::AObstacle()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>("Scene"));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");

	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bEditableWhenInherited = true;
	StaticMesh->OnComponentHit.AddDynamic(this, &AObstacle::OnHitTrigger);
}

// Called when the game starts or when spawned
void AObstacle::BeginPlay()
{
	Super::BeginPlay();

}

void AObstacle::OnHitTrigger(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (ARunCharacter* target = Cast<ARunCharacter>(OtherActor))
		OnTrigger(target);
}

// Called every frame
void AObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

