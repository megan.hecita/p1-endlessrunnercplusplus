// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 *
 */
UCLASS()
class ENDLESSRUNNER_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	ARunGameMode();

private:
	class ATile* SpawnedTile;

	FTransform SpawnPoint;

	FTimerHandle TimerHand;

	FTimerDelegate TimerDel;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ATile> Tile;

	virtual void BeginPlay() override;

	UFUNCTION()
		void SpawnTile();

	UFUNCTION()
		void OnExitTile(ATile* sTile);

	UFUNCTION()
		void DestroyTile(ATile* sTile);
};
