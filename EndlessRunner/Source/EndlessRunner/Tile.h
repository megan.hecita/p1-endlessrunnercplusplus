// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExited, class ATile*, FloorTile);

UCLASS()
class ENDLESSRUNNER_API ATile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATile();

	UPROPERTY(BlueprintAssignable)
		FExited OnExited;

private:

	float ObstacleSpawnChance = 0.6;
	float PickupSpawnChance = 0.3;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UArrowComponent* Arrow;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* BoxCollision;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* BoundingBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<TSubclassOf<class AObstacle>> ObstacleArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<TSubclassOf<class APickup>> PickupArray;

	UPROPERTY()
		AObstacle* obstacleSpawn;

	UPROPERTY()
		APickup* pickupSpawn;

	UFUNCTION()
		void ExitTrigger(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void SpawnObstacle();

	UFUNCTION()
		void SpawnPickup();

	UFUNCTION()
		void DestroySpawnedObjects(AActor* actor);

	FTimerHandle TriggerTimer;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		FTransform GetAttachTransform();
};
