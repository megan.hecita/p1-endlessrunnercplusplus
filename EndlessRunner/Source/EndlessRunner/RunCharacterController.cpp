// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacter.h"
#include "RunCharacterController.h"


ARunCharacterController::ARunCharacterController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	Player = Cast<ARunCharacter>(GetPawn());
}

void ARunCharacterController::MoveRight(float scale)
{
	Player->AddMovementInput(Player->GetActorRightVector(), scale);
}

void ARunCharacterController::PlayerRun()
{
	Player->AddMovementInput(Player->GetActorForwardVector(), 1.0);
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!Player->isDead)
		PlayerRun();
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}
