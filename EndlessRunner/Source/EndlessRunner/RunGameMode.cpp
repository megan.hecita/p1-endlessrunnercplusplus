// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Tile.h"
#include "TimerManager.h" 

ARunGameMode::ARunGameMode()
{

}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	for (int i = 0; i < 5; i++)
	{
		SpawnTile();
	}

}

void ARunGameMode::SpawnTile()
{
	SpawnedTile = GetWorld()->SpawnActor<ATile>(Tile, SpawnPoint);
	SpawnedTile->OnExited.AddDynamic(this, &ARunGameMode::OnExitTile);
	SpawnPoint = SpawnedTile->GetAttachTransform();
}

void ARunGameMode::OnExitTile(ATile* sTile)
{
	SpawnTile();
	TimerDel.BindUFunction(this, FName("DestroyTile"), sTile);
	GetWorld()->GetTimerManager().SetTimer(TimerHand, TimerDel, 1.0f, false);
}

void ARunGameMode::DestroyTile(ATile* sTile)
{
	sTile->Destroy();
}