// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "RunCharacterController.generated.h"


/**
 *
 */
UCLASS()
class ENDLESSRUNNER_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

public:
	ARunCharacterController();

protected:
	virtual void BeginPlay() override;
	class ARunCharacter* Player;

	//Input bindings
	void MoveRight(float scale);

	void PlayerRun();

public:
	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
};
