// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "RunCharacter.h"
#include "Components/ChildActorComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Obstacle.h"
#include "Pickup.h"


// Sets default values
ATile::ATile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>("Scene"));
	Arrow = CreateDefaultSubobject<UArrowComponent>("Arrow");
	BoxCollision = CreateDefaultSubobject<UBoxComponent>("BoxCollision");
	BoundingBox = CreateDefaultSubobject<UBoxComponent>("BoundingBox");

	Arrow->SetupAttachment(RootComponent);
	BoxCollision->SetupAttachment(RootComponent);
	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &ATile::ExitTrigger);
	BoundingBox->SetupAttachment(RootComponent);

	this->OnDestroyed.AddDynamic(this, &ATile::DestroySpawnedObjects);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

	

	if (UKismetMathLibrary::RandomBoolWithWeight(ObstacleSpawnChance))
		SpawnObstacle();

	/*else*/ if (UKismetMathLibrary::RandomBoolWithWeight(PickupSpawnChance))
		SpawnPickup();
}

FTransform ATile::GetAttachTransform()
{
	return FTransform(Arrow->GetComponentTransform());
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATile::ExitTrigger(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* target = Cast<ARunCharacter>(OtherActor))
		OnExited.Broadcast(this);
}

void ATile::SpawnObstacle()
{
	FVector spawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(BoundingBox->GetComponentLocation(), BoundingBox->GetUnscaledBoxExtent());
	int32 index = UKismetMathLibrary::RandomIntegerInRange(0, ObstacleArray.Num() - 1);

	obstacleSpawn = GetWorld()->SpawnActor<AObstacle>(ObstacleArray[index], FTransform(spawnLocation));

	obstacleSpawn->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
}

void ATile::SpawnPickup()
{
	FVector spawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(BoundingBox->GetComponentLocation(), BoundingBox->GetUnscaledBoxExtent());
	int32 index = UKismetMathLibrary::RandomIntegerInRange(0, PickupArray.Num() - 1);

	pickupSpawn = GetWorld()->SpawnActor<APickup>(PickupArray[index], FTransform(spawnLocation));

	pickupSpawn->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
}

void ATile::DestroySpawnedObjects(AActor* actor)
{
	if (obstacleSpawn != nullptr)
		obstacleSpawn->Destroy();

	if (pickupSpawn != nullptr)
		pickupSpawn->Destroy();
}

